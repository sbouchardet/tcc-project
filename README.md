# Introducao

 <p>
 Este projeto esta sendo desenvolvido com o objetivo de criar um sistema que estime a avaliação de filmes baseada em avaliações anteriores.
 Para desenvolver este trabalho é necessario o estudo de práticas de MachineLearning e DataScience para compreender o problema.
 Inicialmente minha ideia é dividir o projeto em 3 fases e documenta-las aqui para observar o andamento das mesmas.
 </p>
 <p> as fases sao: </p>

 <ol>


<li> Desenvolvimento teórico </li>
<p>
Nesta fase pretendo pesquisar algoritimos e métodos relacionados a MachineLearning (Aprendisado de Maquina). Além desse estudo, tambem deverei focar na área de produção de filmes, e as caracteristicas relevantes para julgar um filme. Deverei implementar algoritmos para treinamentos de modelo em python, durante o processo de aprendizado.
</p>

<li> Aquisição de dados </li>
<p>Hoje minha idéia de aquisição de dados é um crawler para o IMDb e adquire as informações dos filmes. Com ajuda da biblioteca <i>pandas</i> do Python, pretendo estruturar essas informações em um DataBase para ser estudado. </p>
<p> É importante que antes dessa fase a fase 1 tenha sido bem desenvolvida, pois no desenvolvimento do DataBse deverei saber as features estudadas para modelar o sistema. Mesmo na fase 2 é importante se manter atualizado ainda executando a fase 1. </p>

<li> Treinamento do modelo </li>
<p> Nesta fase, possuindo certo conhecimento e dados estruturados, deverei treinar um modelo com o DataBase formado na fase 2. Como tenho mais de um modelo de algoritmo para treinamento,  deverei estudar qual dos modelos "<i>aprende melhor</i>", ou seja, tem menor erro. </p>

</ol>

## Desenvolvimento Teórico
<p> Nesta secção pretendo colocar os algoritmos e métodos que aprenderei enquanto estudo MachineLearning. Além disso devo manter os links para as referencias de onde estudo. </p>

<p>
30/04/2016 : Para melhor controlar meu tempo e ter metas irei colocar datas para entrega de metas intemediárias. Notei que preciso revisar ou reler o curso de <a href="https://www.coursera.org/learn/machine-learning">MachineLearning</a> do Coursera que comecei a fazer mas fui interrompida. Decidi revisar as primeiras semanas que ja fiz nessa proxima semana.</br>
<b> Até dia 07/05/2016 (Sábado) deverei ter terminado de revisar e começado a implementar os métodos de MachineLearning abordados no curso </b>
</p>
