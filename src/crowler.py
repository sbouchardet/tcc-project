from bs4 import BeautifulSoup
import requests
import pandas as pd
import datetime

class imdb_crowler:
    __host="http://www.imdb.com/"
    __title=__host+"title/tt%s/"

    def __init__(self,cod):
        self.__title=self.__title%str(cod)
        self.__fullcredits = self.__title+"fullcredits"
        self.__business = self.__title+"business"
        self.__keyword = self.__title+"keywords"
        self.__production_companies = self.__title+"companycredits"
        self.__release_info = self.__title+"releaseinfo"

        self.__soup_title = BeautifulSoup(requests.get(self.__title).text, "html.parser")
        self.__soup_business = None
        self.__soup_release = None
        self.__soup_companies = None
        self.__soup_keyword = None

    def get_title(self):
        return self.__soup_title.find_all('meta',{'property':'og:title'})[0]["content"]

    def __get_born(self,actor_url):
        actor = self.__host+actor_url
        local_soup = BeautifulSoup(requests.get(actor).text,"html.parser")
        try:
            return  datetime.datetime.strptime(local_soup.find('time',attrs={'itemprop':'birthDate'})['datetime'],'%Y-%m-%d')
        except Exception:
            return None

    def get_cast(self):
        table_cast = self.__soup_title.find('table',attrs={'class':'cast_list'})
        result=[]
        try:
            for td in table_cast.find_all('td',attrs={'class':'itemprop'}):
                d = dict()
                d['job']=td['itemprop']
                d['name']= td.find('span').string
                d['born']= self.__get_born(td.find('a')["href"])
                result.append(d)

            other_cast = self.__soup_title.find_all('span',attrs={'itemtype':'http://schema.org/Person'})
            soup_director = filter(lambda x:x['itemprop']!='actors',other_cast)
            soup_director = map(lambda x: {'job':x['itemprop'],'born':self.__get_born(x.find('a')['href']),'name':x.find('span', attrs={'itemprop':"name"}).text},soup_director)

            return result + soup_director

        except Exception:

            return [None]

    def get_business(self):

        def filter_number(n):
            try:
                if(n[0]!="$"):
                     return None
                return int(filter(lambda x: x!=',' and x!='$',n))
            except Exception:
                return None

        self.__soup_business = BeautifulSoup(requests.get(self.__business).text, "html.parser")

        div_content = self.__soup_business.find('div', attrs={'id':'tn15content'})
        if div_content is None:
            return None
        else:
            values = div_content.text.split('\n')[2:6]
            d = {'budget':filter_number(values[0].split(' ')[0]),
                 'opening_weekend':filter_number(values[3].split(' ')[0])}
            return d

    def get_rate(self):
        try:
            rate = self.__soup_title.find('span',attrs={'itemprop':'ratingValue'}).string
            rate = float(rate)
            rate_count = self.__soup_title.find('span',attrs={'itemprop':'ratingCount'}).string
            rate_count = int(filter(lambda x: x!=',',rate_count))
            return {'ratingValue':rate,'ratingCount': rate_count}
        except Exception:
            return {'ratingValue':None,'ratingCount': None}

    def get_duration(self):
        try:
            duration = self.__soup_title.find('time' ,attrs={'itemprop':"duration"})['datetime']
            duration = filter(lambda x: x.isdigit(),duration)
            return int(duration)
        except Exception:
            return None

    def get_genre(self):
        try:
            genres = self.__soup_title.find_all('span' ,attrs={'class':'itemprop','itemprop':'genre'})
            genres_result = map(lambda x: x.text,genres)
            return genres_result
        except Exception:
            return [None]

    def get_companies(self):
        self.__soup_companies = BeautifulSoup(requests.get(self.__production_companies).text,"html.parser")
        try:
            companies_soup = self.__soup_companies.find('ul',attrs={'class':"simpleList"})
            comp = map(lambda x: x.text,companies_soup.find_all('a'))
            return comp
        except Exception:
            return [None]

    def get_release(self):
        dict_months={'January':1,
        'February':2,
        'March':3,
        'April':4,
        'May':5,
        'June':6,
        'July':7,
        'August':8,
        'September':9,
        'October':10,
        'November':11,
        'December':12}

        self.__soup_release = BeautifulSoup(requests.get(self.__release_info).text, "html.parser")

        table_release = self.__soup_release.find('table',attrs={'id':'release_dates'})
        try:
            release_usa = filter(lambda x:x.find('a').text=='USA',table_release.find_all('tr'))[0]
            txt_date= release_usa.find('td',attrs={'class':"release_date"}).text.split(' ')
            day=int(txt_date[0])
            month=dict_months[txt_date[1]]
            year=int(txt_date[2])
            return datetime.datetime(year,month,day)
        except Exception:
            return None

    def get_keyword(self):
        self.__soup_keyword = BeautifulSoup(requests.get(self.__keyword).text,"html.parser")
        try:
            sp_keyword = self.__soup_keyword.find_all('td',{'class':"soda sodavote"})
            kws = map(lambda x: {'vote':x['data-item-votes'],'keyword':x['data-item-keyword']},sp_keyword)
            return kws
        except Exception:
            return [None]
