from utils.db_config import db

class genre:
    def __init__(self,genre_name,cod=-1):
        self.__cod = cod;
        self.genre_name = genre_name;

    def __str__(self):
         return (" (%s)%s " % (str(self.__cod),str(self.genre_name)));

    def get_cod(self):
        return self.__cod;

    def set_cod(self):
        if self.__cod==-1:
            d = db();
            cod = d.get_cod("genre",["genre_id", "genre_name"],[self.genre_name])
            if cod==-1:
                cod = self.__add_db(d)
            del d
            self.__cod = cod;

    def __add_db(self,d):
        d.insert("genre",["genre_name"],[self.genre_name]);
        c = d.get_cod("genre",["genre_id", "genre_name"],[self.genre_name])
        return c
