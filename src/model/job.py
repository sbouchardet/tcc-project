#!../../.env/bin/python
from utils.db_config import db
from job_type import job_type
from person import person
class job:
    person = person()
    def __init__(self, person, job_type):
        self.person = person;
        self.job_type = job_type;

    def __str__(self):
        return "%s - %s"%(str(self.person),str(self.job_type))
