from utils.db_config import db

class job_type:
    __cod=-1;

    def __init__(self,name):
        self.__cod=-1
        self.__name=name

    def __str__(self):
        return "(%s)%s"%(str(self.__cod),self.__name)

    def set_cod(self):
        if self.__cod==-1:
            d = db()
            cod = d.get_cod("person_job",["job_id","job"],[self.__name])
            if cod ==-1:
                cod = self.__add_db(d)
            del d
            self.__cod = cod


    def get_cod(self):
        return self.__cod

    def __add_db(self,d):
        d.insert("person_job",["job"],[self.__name])
        cod = d.get_cod("person_job",["job_id","job"],[self.__name])
        return cod
