from utils.db_config import db

class keyword:
    __cod=-1
    def __init__(self, word, cod = -1):
        self.__keyword=word
        self.__cod = cod;

    def __str__(self):
        return "(%s)%s"%(self.__cod,self.__keyword)

    def set_cod(self):
        if self.__cod==-1:
            d = db()
            cod = d.get_cod("movie_keywords",["keyword_id","keyword"],[self.__keyword])
            if cod ==-1:
                cod = self.__add_db(d)
            del d
            self.__cod=cod

    def get_cod(self):
        return self.__cod

    def __add_db(self,d):
        d.insert("movie_keywords",["keyword"],[self.__keyword])
        cod = d.get_cod("movie_keywords",["keyword_id","keyword"],[self.__keyword]);
        return cod
