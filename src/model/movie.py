
from person import person
from genre import genre
from production import production
from job import job
from job_type import job_type
import datetime

from utils.db_config import db

class movie:

    def __init__(self, name):
        self.__name = name
        self.__cod=None
        self.__cast=[]
        self.__genres=[]
        self.__keyword=[]
        self.__production=[]
        self.__duration_min=None
        self.__release_date=None;
        self.__budget=None;
        self.__opening_weekend=None;
        self.__rate=None
        self.__count_rate=None

    def __del__(self):
        __cast=[]
        __genres=[]
        __keyword=[]
        __production=[]

    def __str__(self):
        def map_to_str(a):
            return map(lambda x: str(x),a)

        d={"name":self.__name,
        "cast":map_to_str(self.__cast),
        "keyword":map_to_str(self.__keyword),
        "production":map_to_str(self.__production),
        "duration":self.__duration_min,
        "budget":self.__budget,
        "opening_weekend":self.__opening_weekend,
        "genre":map_to_str(self.__genres)}

        return str(d)


    def set_budget(self,val):
        self.__budget=val

    def set_rate(self,val):
        self.__rate=val
    def set_count_rate(self,val):
        self.__count_rate=val

    def set_opening_weekend(self,val):
        self.__opening_weekend=val

    def set_release_date(self, date):
        self.__release_date=date

    def set_duration(self,val):
        self.__duration_min=val

    def set_job(self, person, job_type):
        person.set_cod()
        job_type.set_cod()
        self.__cast.append(job(person,job_type))

    def set_genre(self,genre):
        genre.set_cod();
        self.__genres.append(genre)

    def set_keyword(self,kw,vote):
        kw.set_cod()
        self.__keyword.append([kw,vote]);


    def set_production(self, pdct):
        pdct.set_cod();
        self.__production.append(pdct)

    def set_cod(self,forceAdd=False):
        isNew = True
        if forceAdd:
            d=db()
            self.__update_bd(d);
            del d

        if self.__cod is None:
            d=db()

            cod = d.get_cod("movie",["movie_id","title"],[self.__name])

            if cod==-1 :
                cod = self.__add_bd(d);
            else:
                isNew=False

            self.__cod = cod;
            del d

        return (isNew,self.__cod)

    def get_cod(self):
        if self.__cod==-1:
            return self.set_cod()

        return self.__cod

    def __add_bd(self,d):

        if self.__release_date is not None:
            release_date_str = "%s-%02d-%02d"%(self.__release_date.year,self.__release_date.month,self.__release_date.day)
        else:
            release_date_str=None

        schema_db = ["title","duration","release_date","budget","opening_weekend","rate","count_rate"]
        value_db = [self.__name,self.__duration_min,release_date_str,self.__budget,self.__opening_weekend, self.__rate, self.__count_rate]
        zip_to_filter = zip(schema_db,value_db)
        filtered = filter(lambda x:x[1] is not None,zip_to_filter)
        unzip = zip(*filtered)

        schema_db = unzip[0]
        value_db = unzip[1]

        d.insert("movie",schema_db,value_db)
        cod = d.get_cod("movie",["movie_id","title"],[value_db[0]])
        return cod

    def __update_bd(self,d):

        if self.__release_date is not None:
            release_date_str = "%s-%02d-%02d"%(self.__release_date.year,self.__release_date.month,self.__release_date.day)
        else:
            release_date_str=None

        schema_db = ["movie_id","title","duration","release_date","budget","opening_weekend","rate","count_rate"]
        value_db = [self.__cod,self.__name,self.__duration_min,release_date_str,self.__budget,self.__opening_weekend, self.__rate, self.__count_rate]
        zip_to_filter = zip(schema_db,value_db)
        filtered = filter(lambda x:x[1] is not None,zip_to_filter)
        unzip = zip(*filtered)

        schema_db = unzip[0]
        value_db = unzip[1]

        d.update("movie",schema_db,value_db)
        cod = d.get_cod("movie",["movie_id","title"],[value_db[0]])
        return cod


    def insert_db(self, forceAdd):

        isNew , c = self.set_cod(forceAdd)
        print "%s - %s"% (c,self.__name)

        d = db()
        movie_cod=self.get_cod()

        for j in self.__cast:
            p = j.person.get_cod()
            j_type = j.job_type.get_cod()
            try:
                d.insert("r_person_movie",["job_id","person_id","movie_id"],[j_type,p,movie_cod])
            except Exception:
                d.console_log("Duplicate Row!")

        for g in self.__genres:
            cod_genre = g.get_cod()
            try:
                d.insert("r_genre_movie",["genre_id","movie_id"],[cod_genre,movie_cod])
            except Exception:
                d.console_log("Duplicate Row!")

        for k in self.__keyword:
            cod_k = k[0].get_cod();
            try:
                d.insert("r_keyword_movie",["keyword_id","movie_id","vote"],[cod_k,movie_cod,k[1]])
            except Exception:
                d.console_log("Duplicate Row!")


        for p in self.__production:
            cod_p = p.get_cod()
            try:
                d.insert("r_movie_productor",["production_id","movie_id"],[cod_p,movie_cod])
            except Exception:
                d.console_log("Duplicate Row!")

        del d
