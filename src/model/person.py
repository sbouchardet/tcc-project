#!../../.env/bin/python
import datetime
from utils.db_config import db

class person:
    __cod=-1
    def __init__(self,name="",born=None):
        self.__name=name
        self.__born = born

    def __str__(self):
        return "(%s)%s"%(self.__cod,self.__name)

    def set_cod(self):
        d = db();
        cod = d.get_cod("person",["person_id", "person_name"],[self.__name])
        if(cod==-1):
            cod=self.__add_bd(d)
        del d
        self.__cod = cod;

    def get_cod(self):
        return self.__cod;

    def __add_bd(self, d):
        c=0
        if self.__born is not None:
            str_born="%s-%s-%s"%(self.__born.year,self.__born.month,self.__born.day)
        else:
            str_born=None

        d.insert("person",["person_name","person_born"],[self.__name,str_born])
        c = d.get_cod("person",["person_id","person_name","person_born"],[self.__name,str_born])


        return c

    @staticmethod
    def search_by_day(day):
        day_iso = "%s-%s-%s"%(day.year,day.month, day.day)
        db_handler = db();
        result = db_handler.exc_query("select person_id,person_name from person where person_born=\'%s\'"%day_iso)
        r=[];
        for i in result:
            d=dict()
            d['person_id']=i[0]
            d['person_name']=i[1]
            r.append(d)
        del db_handler
        return r
