import sys
from utils.db_config import db


class production:

    def __init__(self,prod_name="",cod=-1):
        self.prod_name = prod_name
        self.__cod=cod

    def __str__(self):
        return self.prod_name

    def set_cod(self):
        if self.__cod==-1:
            d=db()
            cod = d.get_cod("production_companie",["production_id", "productor"],[self.prod_name])
            if cod==-1:
                cod = self.__add_bd(d);
            del d
            self.__cod = cod;

    def get_cod(self):
        return self.__cod

    def __add_bd(self,d):
        d.insert("production_companie",["productor"],[self.prod_name]);
        c = d.get_cod("production_companie",["production_id", "productor"],[self.prod_name])
        del d
        return c
