#!../../.env/bin/python
import MySQLdb

def filter_char(string):
    if isinstance(string, basestring) :
        return "".join(filter(lambda y: y!='\"',string))
    else :
        return string

class db:
    __host="localhost"
    __user="root"
    __password="";
    __db_name = "movies";

    def __init__(self, debug=False):
        self.__debug=debug
        self.console_log('Start Connection...')
        self.__db = MySQLdb.connect(host=self.__host, user=self.__user, passwd=self.__password,
        db=self.__db_name)
        self.__cursor = self.__db.cursor()

    def __del__(self):
        self.console_log('...End Connection!')
        self.__db.commit();
        self.__cursor.close();
        self.__db.close();

    def console_log(self,msg):
        if self.__debug:
            print msg

    def insert(self, table_name, table_schema, table_values, trace=False):
        template = 'insert into %s (%s) values (%s);'
        table_values = map(lambda x:filter (lambda y: y!='\"',x) if isinstance(x, basestring) else x , table_values)
        zip_schema_val = zip(table_schema,table_values)
        filt_schema_val=filter(lambda x: x[1]is not None,zip_schema_val)
        unzip=zip(*filt_schema_val)

        tb_schema_concat = ",".join(unzip[0])
        tb_values_concat = ",".join(map(lambda x: "\"%s\""%x,unzip[1]))
        sql_cmd = template%(table_name,tb_schema_concat,tb_values_concat)
        self.console_log(sql_cmd)
        if( not trace):
             self.__cursor.execute(sql_cmd);

    def list(self, table_name, table_schema):
        cmd = "select %s from %s"%(",".join(table_schema),table_name)
        self.__cursor.execute(cmd);
        result = self.__cursor.fetchall()
        r=[];
        for row in result:
            loc=dict();
            for i in range(len(table_schema)):
                loc[table_schema[i]]=row[i];
            r.append(loc);
        return r;

    def update(self, table_name, table_schema, table_values, trace=False):
        template = 'update %s set %s where %s ;'
        table_values = map(lambda x:filter (lambda y: y!='\"',x) if isinstance(x, basestring) else x , table_values)
        zip_schema_val = zip(table_schema,table_values)
        filt_schema_val=filter(lambda x: x[1]is not None,zip_schema_val)

        set_values = map(lambda x: "%s=\"%s\""%(x[0],filter_char(x[1])),filt_schema_val)

        tb_schema_concat = ",".join(set_values)

        sql_cmd = template%(table_name,tb_schema_concat,set_values[0])
        self.console_log(sql_cmd)
        if( not trace):
             self.__cursor.execute(sql_cmd);

    def get_cod(self, table_name,table_schema,table_values):
        cmd="select %s from %s where %s"
        table_schema_filter = table_schema[1:]
        r = zip(table_schema_filter,table_values)
        conditions = map(lambda x: "%s=\"%s\""%(x[0],filter_char(x[1])) if x[1] is not None else "%s is null"%(x[0]),r)
        comand = cmd%(table_schema[0],table_name," and ".join(conditions))
        self.console_log(comand)

        self.__cursor.execute(comand)
        row = self.__cursor.fetchone()
        if row is not None:
            if len(row)==1:
                return row[0]
            else:
                return -1;
        else :
            return -1;

    def exc_query(self,query):
        self.__cursor.execute(query)
        rows = self.__cursor.fetchall()
        return rows
