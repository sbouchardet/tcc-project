import datetime
import time
from random import randint
from bs4 import BeautifulSoup
import requests
import time
import timeout_decorator


from model.production import production
from model.movie import movie
from model.person import person
from model.job import job
from model.genre import genre
from model.keyword import keyword
from model.job_type import job_type
from crowler import imdb_crowler

def timeout_handle(movie_id):
    with open("movies_timeout.txt", "a") as myfile:
        myfile.write("\n"+str(movie_id))


@timeout_decorator.timeout(300)
def save_movie(cod):
    dory = imdb_crowler(cod)
    m=None
    m = movie(dory.get_title())

    isNew , c = m.set_cod()

    if isNew:
        print "[ NEW ] "
        m.set_release_date(dory.get_release())
        r = dory.get_rate()

        m.set_rate(r['ratingValue'])
        m.set_count_rate(r['ratingCount'])
        d_business=dory.get_business()
        m.set_budget(d_business['budget'])
        m.set_opening_weekend(d_business['opening_weekend'])
        m.set_duration(dory.get_duration())

        for prod in dory.get_companies():
            if prod is not None:
                m.set_production(production(prod))

        for pj in dory.get_cast():
            if pj is not None:
                m.set_job(person=person(pj['name'],pj['born']),job_type=job_type(pj['job']))

        for g in dory.get_genre():
            if g is not None:
                m.set_genre(genre=genre(g))

        for k in dory.get_keyword():
            if k is not None:
                m.set_keyword(keyword(k['keyword']),k['vote'])

        m.insert_db(True)


    del m

# ls031465953 | 73
# ls076596691 | 40
# ls070349009 | 210
# ls071296235 | 43
# ls053298079 | 210
# ls009013813 | 172
# ls056399999 | 50
# ls000946252 | 14
# ls071101760 | 14
# ls071911348 | 39
# ls008304647 | 47
# ls076647887 | 27
# ls059090909 | 6
# ls006502992 | 50
# ls059751451 | 51
# ls052818977 | 10
# ls003157923 | 50
# ls052367650 | 37
# ls059090375 | 10
# ls054368176 | 10
# ls053690276 | 200
# ls050338253 | 199
# ls003539266 | 43
# ls003750175 | 88
# ls051264982 | 236
# ls051222845 | 146
# ls051268784 | 230
# ls051241190 | 176
# ls051246880 | 177
# ls003067540 | 100
# ls000098551 | 198
# ls000163210 | 374
# ls009492400 | 868
# ls006266261 | 1000
# ls051343964 | 114

start_val = 0;
for il in range (0,6):
    url_list= "http://www.imdb.com/list/ls057083439/?start=%s&view=compact"%(str(start_val + 250*il))
    resultText = requests.get(url_list).text;
    ids_bs = BeautifulSoup(resultText,"html.parser");
    ids = ids_bs.find_all('td',{'class':'title'});


    movies=[]
    for tt in ids:
        movies.append(int(filter(lambda x: x.isdigit(),tt.find('a')['href'])));

    print movies;

    for i in movies:
        st = time.time()
        # r = randint(1,2500000)
        r=i
        print "movie id %s"%r
        try:
            save_movie(r)
        except timeout_decorator.TimeoutError:
            timeout_handle(r)

        end = time.time()
        print"%ss"%(end-st)
