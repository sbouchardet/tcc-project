# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.32)
# Database: movies
# Generation Time: 2017-01-14 23:44:43 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table genre
# ------------------------------------------------------------

DROP TABLE IF EXISTS `genre`;

CREATE TABLE `genre` (
  `genre_id` int(11) NOT NULL AUTO_INCREMENT,
  `genre_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;



# Dump of table movie
# ------------------------------------------------------------

DROP TABLE IF EXISTS `movie`;

CREATE TABLE `movie` (
  `movie_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `release_date` date DEFAULT NULL,
  `budget` decimal(21,2) DEFAULT NULL,
  `opening_weekend` decimal(21,2) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `count_rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`movie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6290 DEFAULT CHARSET=utf8;



# Dump of table movie_keywords
# ------------------------------------------------------------

DROP TABLE IF EXISTS `movie_keywords`;

CREATE TABLE `movie_keywords` (
  `keyword_id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`keyword_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66033 DEFAULT CHARSET=utf8;



# Dump of table person
# ------------------------------------------------------------

DROP TABLE IF EXISTS `person`;

CREATE TABLE `person` (
  `person_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_name` varchar(255) DEFAULT NULL,
  `person_born` date DEFAULT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=58134 DEFAULT CHARSET=utf8;



# Dump of table person_job
# ------------------------------------------------------------

DROP TABLE IF EXISTS `person_job`;

CREATE TABLE `person_job` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;



# Dump of table production_companie
# ------------------------------------------------------------

DROP TABLE IF EXISTS `production_companie`;

CREATE TABLE `production_companie` (
  `production_id` int(11) NOT NULL AUTO_INCREMENT,
  `productor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`production_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8340 DEFAULT CHARSET=utf8;



# Dump of table r_genre_movie
# ------------------------------------------------------------

DROP TABLE IF EXISTS `r_genre_movie`;

CREATE TABLE `r_genre_movie` (
  `genre_id` int(11) DEFAULT NULL,
  `movie_id` int(11) DEFAULT NULL,
  KEY `fk_genre` (`genre_id`),
  KEY `fk_movie_genre` (`movie_id`),
  CONSTRAINT `fk_genre` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`genre_id`),
  CONSTRAINT `fk_movie_genre` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table r_keyword_movie
# ------------------------------------------------------------

DROP TABLE IF EXISTS `r_keyword_movie`;

CREATE TABLE `r_keyword_movie` (
  `keyword_id` int(11) DEFAULT NULL,
  `movie_id` int(11) DEFAULT NULL,
  `vote` int(11) DEFAULT NULL,
  KEY `fk_keyword` (`keyword_id`),
  KEY `fk_movie` (`movie_id`),
  CONSTRAINT `fk_keyword` FOREIGN KEY (`keyword_id`) REFERENCES `movie_keywords` (`keyword_id`),
  CONSTRAINT `fk_movie` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table r_movie_productor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `r_movie_productor`;

CREATE TABLE `r_movie_productor` (
  `production_id` int(11) DEFAULT NULL,
  `movie_id` int(11) DEFAULT NULL,
  KEY `fk_prod` (`production_id`),
  KEY `fk_prod_movie` (`movie_id`),
  CONSTRAINT `fk_prod` FOREIGN KEY (`production_id`) REFERENCES `production_companie` (`production_id`),
  CONSTRAINT `fk_prod_movie` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table r_person_movie
# ------------------------------------------------------------

DROP TABLE IF EXISTS `r_person_movie`;

CREATE TABLE `r_person_movie` (
  `job_id` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `movie_id` int(11) DEFAULT NULL,
  KEY `fk_job_person` (`job_id`),
  KEY `fk_person_movie` (`person_id`),
  KEY `fk_movie_person` (`movie_id`),
  CONSTRAINT `fk_job_person` FOREIGN KEY (`job_id`) REFERENCES `person_job` (`job_id`),
  CONSTRAINT `fk_movie_person` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`) ON DELETE CASCADE,
  CONSTRAINT `fk_person_movie` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
